class Logger {
    constructor(name) {
        this.name = name;
        this.count = 0;
    }

    log(message) {
        this.count++;
        console.log(`[${this.name}] ${message} Count: ${this.count}`);
    }

    info(message) {
        this.log(`info: ${message}`);
    }

    verbose(message) {
        this.log(`verbose: ${message}`);
    }
}

module.exports = Logger;
