const os = require('os');
// получим имя текущего пользователя
const userName = os.userInfo().username;
const express = require('express');
const router = express.Router();

//const path = require('path');
//const fs = require('fs');

const util = require("util");
const Emitter = require("events");
//const emitter = new Emitter();
const eventName = "greet";

function User(){}

util.inherits(User, Emitter);
User.prototype.sayHi = function(data){
    this.emit(eventName, data);
}
const user = new User();
// добавляем к объекту user обработку события "greet"
user.on(eventName, function(data){
    console.log(data);
});
user.sayHi("Мне нужна твоя одежда...");
// emitter.on(eventName, function(){
//     console.log("Hello all!");
// });
//
// emitter.on(eventName, function(data){
//     console.log("Привет!", data);
// });
//
// emitter.emit(eventName, ' Hello!');

//console.log(path.basename('hello.txt'));
// асинхронное чтение
// fs.readFile(path.join(__dirname, '/hello.txt'), "utf8",
//     function(error,data){
//         console.log("Асинхронное чтение файла");
//         if(error) throw error; // если возникла ошибка
//         console.log(data);  // выводим считанные данные
//          });

// fs.appendFile(path.join(__dirname, '/hello.txt'), "Привет МИД!", (error) => {
//     if(error) throw error; // если возникла ошибка
//
//     console.log("Запись файла завершена. Содержимое файла:");
//     const data = fs.readFileSync(path.join(__dirname, '/hello.txt'), "utf8");
//     console.log(data);  // выводим считанные данные
// });

// синхронное чтение
// console.log('Синхронное чтение файла');
// const fileContent = fs.readFileSync(path.join(__dirname, '/hello.txt'), "utf8");
// console.log(fileContent);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: userName === 'denis' ? 'DenisK' : 'Some user'});
});

const request = require('request');
const fs = require('fs');
const mkdirp = require('mkdirp');
const path = require('path');
const utilities = require('./utilities');

function spider(url, callback) {
    const filename = utilities.urlToFilename(url);
    fs.access(filename, err => {
        if (err) {
            console.log(`Downloading ${url}`);
            request(url, (error, response, body) => {      //[2]
                if(error) {
                    callback(error);
                } else {
                    mkdirp(path.dirname(filename), err => {    //[3]
                        if(err) {
                            callback(err);
                        } else {
                            fs.writeFile(filename, body, err => { //[4]
                                if(err) {
                                    callback(err);
                                } else {
                                    callback(null, filename, true);
                                }
                            });
                        }
                    });
                }
            });
        } else {
            callback(null, filename, false);
        }
    });
}

spider('http://shop.low-battery.ru/', (err, filename, downloaded) => {
    if(err) {
        console.log(err);
    } else if(downloaded){
        console.log(`Completed the download of "${filename}"`);
    } else {
        console.log(`"${filename}" was already downloaded`);
    }
});


module.exports = router;
